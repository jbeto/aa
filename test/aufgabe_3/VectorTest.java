// Autor: Edgar Toll
// Nach dem Beispiel der trace.txt
package aufgabe_3;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class VectorTest {

    private final Vector u = CreateVector(1, 2, 3);
    private final Vector v = CreateVector(4, 5, 6);

    private final Vector a1 = CreateVector(2, 2);
    private final Vector b1 = CreateVector(0, 0);
    private final Vector p1 = CreateVector(0, 1);

    private final Vector a2 = CreateVector(1, 2);
    private final Vector b2 = CreateVector(1, 1);
    private final Vector p2 = CreateVector(2, 1);

    private final Vector u2 = CreateVector(1, 0, 0);
    private final Vector v2 = CreateVector(0, 1, 0);

    private Vector CreateVector(double... values) {
        return new Vector(values);
    }

    @Test
    public void testArrayCopyConstructor() {
        //Arrays.copyOf verwenden
        double[] testArray = new double[]{1, 2, 3};

        Vector testVector = CreateVector(testArray);

        testArray[0] = 0;

        assertEquals(1, testVector.getA()[0], Math.ulp(1));
    }

    @Test
    public void testArrayCopyGetA() {
        //Arrays.copyOf verwenden
        Vector testVector = CreateVector(1, 2, 3);
        double[] testArray = testVector.getA();

        testArray[0] = 0;

        assertEquals(1, testVector.getA()[0], Math.ulp(1));
    }

    @Test
    public void testGenerationDim() {
        assertEquals(3, u.getDim());
        assertEquals(3, v.getDim());

        assertEquals(2, a1.getDim());
        assertEquals(2, b1.getDim());
        assertEquals(2, p1.getDim());

        assertEquals(3, u2.getDim());
        assertEquals(3, v2.getDim());
    }

    @Test
    public void testGenerationA() {
        assertArrayEquals(new double[]{1, 2, 3}, u.getA(), Math.ulp(3));
        assertArrayEquals(new double[]{4, 5, 6}, v.getA(), Math.ulp(6));

        assertArrayEquals(new double[]{2, 2}, a1.getA(), Math.ulp(3));
        assertArrayEquals(new double[]{0, 0}, b1.getA(), Math.ulp(6));
        assertArrayEquals(new double[]{0, 1}, p1.getA(), Math.ulp(6));

        assertArrayEquals(new double[]{1, 0, 0}, u2.getA(), Math.ulp(3));
        assertArrayEquals(new double[]{0, 1, 0}, v2.getA(), Math.ulp(6));
    }

    @Test
    public void testToString() {
        assertEquals("(1.0,2.0,3.0)", u.toString());
        assertEquals("(4.0,5.0,6.0)", v.toString());

        assertEquals("(2.0,2.0)", a1.toString());
        assertEquals("(0.0,0.0)", b1.toString());
        assertEquals("(0.0,1.0)", p1.toString());

        assertEquals("(1.0,0.0,0.0)", u2.toString());
        assertEquals("(0.0,1.0,0.0)", v2.toString());
    }

    @Test
    public void testEquals() {
        assertEquals(false, u.equals(null));
        assertEquals(false, u.equals(42));
        assertEquals(false, u.equals(v));
        assertEquals(true, u.equals(CreateVector(1, 2, 3)));
    }

    @Test
    public void testAdd() {
        assertArrayEquals(new double[]{5, 7, 9}, u.add(v).getA(), Math.ulp(9));
    }

    @Test
    public void testSub() {
        assertArrayEquals(new double[]{-3, -3, -3}, u.sub(v).getA(), Math.ulp(-3));
    }

    @Test
    public void testSkalarMult() {
        assertArrayEquals(new double[]{5, 10, 15}, u.scalarMult(5).getA(), Math.ulp(15));
    }

    @Test
    public void testSkalarProduct() {
        assertEquals(32, u.scalarProduct(v), Math.ulp(32));
    }

    @Test
    public void testNorm() {
        assertEquals(3.7416573867739413, u.norm(), Math.ulp(3.7416573867739413));
        assertEquals(8.774964387392123, v.norm(), Math.ulp(8.774964387392123));
    }

    @Test
    public void testDistanceToLine() {
        assertEquals(0.7071067811865476, Vector.distanceToLine(a1, b1, p1), Math.ulp(0.7071067811865476));
        assertEquals(2 / Math.sqrt(5), Vector.distanceToLine(a2, b2, p2), Math.ulp(2 / Math.sqrt(5)));
    }

    @Test
    public void testAngle() {
        assertEquals(90, Vector.angle(u2, v2), Math.ulp(90));

        //own test
        assertEquals(69.5, Vector.angle(CreateVector(1, 4, -2), CreateVector(-3, 3, 1)), 0.1);
    }

    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
