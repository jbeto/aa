/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aufgabe_4;

import aufgabe_3.Vector;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author edjopato
 */
public class MatrixTest {

    @Test
    public void CanInit() {
        Matrix myMatrix = new Matrix(new Vector(0, 1, 2));
    }

    @Test
    public void MatrixSize() {
        Matrix myMatrix = new Matrix(new Vector(0, 1, 2), new Vector(3, 4, 5));

        assertEquals(2, myMatrix.getWidth());
        assertEquals(3, myMatrix.getHeight());

        assertArrayEquals(new double[]{0, 1, 2}, myMatrix.getVectors()[0].getA(), Math.ulp(2));
        assertArrayEquals(new double[]{3, 4, 5}, myMatrix.getVectors()[1].getA(), Math.ulp(5));
    }

    @Test
    public void Transform() {
        Matrix myMatrix = new Matrix(new Vector(0, 1, 2), new Vector(3, 4, 5)).transform();

        assertEquals(3, myMatrix.getWidth());
        assertEquals(2, myMatrix.getHeight());

        assertArrayEquals(new double[]{0, 3}, myMatrix.getVectors()[0].getA(), Math.ulp(3));
        assertArrayEquals(new double[]{1, 4}, myMatrix.getVectors()[1].getA(), Math.ulp(4));
        assertArrayEquals(new double[]{2, 5}, myMatrix.getVectors()[2].getA(), Math.ulp(5));
    }

    @Test
    public void SimpleMult() {
        Matrix myMatrix = new Matrix(new Vector(0, 1, 2), new Vector(3, 4, 5));

        myMatrix = myMatrix.mult(2);

        assertArrayEquals(new double[]{0, 2, 4}, myMatrix.getVectors()[0].getA(), Math.ulp(4));
        assertArrayEquals(new double[]{6, 8, 10}, myMatrix.getVectors()[1].getA(), Math.ulp(10));
    }

    @Test
    public void MatrixMult() {
        Matrix a = new Matrix(new Vector(4, 0), new Vector(2, -2), new Vector(1, 4));
        Matrix b = new Matrix(new Vector(1, 0, 2), new Vector(2, 4, -1), new Vector(3, 6, 8));

        Matrix resultMatrix = a.mult(b);

        assertArrayEquals(new double[]{6, 8}, resultMatrix.getVectors()[0].getA(), Math.ulp(8));
        assertArrayEquals(new double[]{15, -12}, resultMatrix.getVectors()[1].getA(), Math.ulp(15));
        assertArrayEquals(new double[]{32, 20}, resultMatrix.getVectors()[2].getA(), Math.ulp(32));
    }

    @Test
    public void MatrixMultTargetPosition() {
        Matrix a = new Matrix(new Vector(4, 0), new Vector(2, -2), new Vector(1, 4));
        Matrix b = new Matrix(new Vector(1, 0, 2), new Vector(2, 4, -1), new Vector(3, 6, 8));

        assertArrayEquals(new double[]{4, 0, 2}, a.multTargetPosition(b, 0, 0), Math.ulp(8));
        assertArrayEquals(new double[]{8, 8, -1}, a.multTargetPosition(b, 1, 0), Math.ulp(15));
        assertArrayEquals(new double[]{0, -12, 32}, a.multTargetPosition(b, 2, 1), Math.ulp(32));

    }

    @Test
    public void IdentityTest() {
        assertEquals(true, new Matrix(new Vector(1)).isIdentity());
        assertEquals(true, new Matrix(new Vector(1, 0), new Vector(0, 1)).isIdentity());
        assertEquals(true, new Matrix(new Vector(1, 0, 0), new Vector(0, 1, 0), new Vector(0, 0, 1)).isIdentity());

        assertEquals(false, new Matrix(new Vector(0)).isIdentity());
        assertEquals(false, new Matrix(new Vector(0, 1, 2), new Vector(3, 4, 5)).isIdentity());
    }

    @Test
    public void OrthogonalTest() {
        double bla = 1 / Math.sqrt(2);

        assertEquals(true, new Matrix(new Vector(bla, bla), new Vector(-1 * bla, bla)).isOrthogonal());
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
