/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aufgabe_4;

import aufgabe_3.Vector;
import java.util.Arrays;

/**
 *
 * @author edjopato
 */
public class Matrix {

    private final Vector[] vectors;

    public int getWidth() {
        return vectors.length;
    }

    public int getHeight() {
        return vectors[0].getDim();
    }

    public Vector[] getVectors() {
        return Arrays.copyOf(vectors, vectors.length);
    }

    public Matrix(Vector... vectors) {
        if (vectors == null) {
            throw new IllegalArgumentException("null nicht erlaubt");
        }
        if (vectors.length == 0) {
            throw new IllegalArgumentException("muss mindestens ein Element enthalten");
        }

        int firstVectorDim = vectors[0].getDim();
        for (int i = 1; i < vectors.length; i++) {
            if (vectors[i].getDim() != firstVectorDim) {
                throw new IllegalArgumentException("Alle Vektoren müssen die selbe Dimension haben");
            }
        }

        this.vectors = Arrays.copyOf(vectors, vectors.length);
    }

    public Matrix(double[][] elements) {
        Vector[] myVectors = new Vector[elements.length];

        for (int x = 0; x < elements.length; x++) {
            myVectors[x] = new Vector(elements[x]);
        }

        vectors = myVectors;
    }

    public Matrix transform() {
        Vector[] newVectors = new Vector[getHeight()];
        double[] newVector = new double[getWidth()];

        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                newVector[x] = vectors[x].getA()[y];
            }
            newVectors[y] = new Vector(newVector);
        }

        return new Matrix(newVectors);
    }

    public Matrix mult(double factor) {
        Vector[] newVectors = getVectors();

        for (int i = 0; i < newVectors.length; i++) {
            newVectors[i] = newVectors[i].scalarMult(factor);
        }

        return new Matrix(newVectors);
    }

    public Matrix mult(Matrix secondMatrix) {
        Matrix firstMatrix = transform();

        Vector[] newVectors = new Vector[secondMatrix.getWidth()];
        double[] newVector = new double[getHeight()];

        for (int x = 0; x < secondMatrix.getWidth(); x++) {
            Vector secondVector = secondMatrix.getVectors()[x];
            for (int y = 0; y < getHeight(); y++) {
                newVector[y] = firstMatrix.getVectors()[y].scalarProduct(secondVector);
            }

            newVectors[x] = new Vector(newVector);
        }

        return new Matrix(newVectors);
    }

    public double[] multTargetPosition(Matrix secondMatrix, int x, int y) {
        double[] elementsOfFirst = new double[getWidth()];
        double[] elementsOfSecond = new double[secondMatrix.getHeight()];

        for (int i = 0; i < elementsOfFirst.length; i++) {
            elementsOfFirst[i] = vectors[i].getA()[y];
        }

        elementsOfSecond = Arrays.copyOf(secondMatrix.getVectors()[x].getA(), elementsOfSecond.length);

        for (int i = 0; i < elementsOfFirst.length; i++) {
            elementsOfFirst[i] *= elementsOfSecond[i];
        }

        return elementsOfFirst;
    }

    public boolean isOrthogonal() {
        Vector[] newVectors = new Vector[getWidth()];
        double[] newVector = new double[getHeight()];
        double biggestNumber = Double.MIN_VALUE;

        for (int x = 0; x < getWidth(); x++) {
            Vector secondVector = vectors[x];
            for (int y = 0; y < getHeight(); y++) {
                double current = vectors[y].scalarProduct(secondVector);

                if (Math.abs(current) > biggestNumber) {
                    biggestNumber = Math.abs(current);
                }

                newVector[y] = current;
            }

            newVectors[x] = new Vector(newVector);
        }

        return new Matrix(newVectors).isIdentity();
    }

    public boolean isIdentity() {
        if (getWidth() != getHeight()) {
            return false;
        }

        for (int x = 0; x < getWidth(); x++) {
            double[] currentVectorA = vectors[x].getA();
            for (int y = 0; y < getWidth(); y++) {
                double element = currentVectorA[y];
                double up = element + 2 * Math.ulp(element);
                double down = element - 2 * Math.ulp(element);

                if (x == y) {
                    if (up < 1 || down > 1) {
                        return false;
                    }
                } else {
                    if (up < 0 || down > 0) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public double[][] getAsArray() {
        double[][] returnArray = new double[getWidth()][getHeight()];

        for (int x = 0; x < getWidth(); x++) {
            double[] currentVectorA = vectors[x].getA();
            for (int y = 0; y < getWidth(); y++) {
                returnArray[x][y] = currentVectorA[y];
            }
        }

        return returnArray;
    }

    @Override
    public String toString() {
        String returnString = "";
        Matrix transformed = transform();

        for (int i = 0; i < getHeight(); i++) {
            if (i != 0) {
                returnString += "\r\n";
            }

            returnString += Arrays.toString(transformed.getVectors()[i].getA());
        }

        return returnString;
    }

    public static void main(String[] args) {
        Matrix a = new Matrix(new Vector(1 / 2.0, 1 / 2.0, 1 / 2.0, 1 / 2.0), new Vector(1 / 2.0, -5 / 6.0, 1 / 6.0, 1 / 6.0), new Vector(1 / 2.0, 1 / 6.0, 1 / 6.0, -5 / 6.0), new Vector(1 / 2.0, 1 / 6.0, -5 / 6.0, 1 / 6.0));

        System.out.println(a);
        System.out.println("\r\n");

        int x = 2;
        int y = 1;

        System.out.println(x + ", " + y + ":");

        System.out.println(Arrays.toString(a.multTargetPosition(a, x, y)));

        System.out.println("\r\n");
        bla();
    }

    private static void bla() {
        Vector a = new Vector(1, 2);
        Vector b = new Vector(1, 1);
        Vector P = new Vector(2, 1);

        double d = Vector.distanceToLine(a, b, P);
        double sqrt5 = Math.sqrt(5);

        double test = d * sqrt5;
    }
}
