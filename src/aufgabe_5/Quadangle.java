package aufgabe_5;

import java.awt.Graphics;

/**
 * *
 *
 * Zeichnet ein Rectieck in ein (unsichtbares) Koordinatensystem mit Ursprung in
 * xn = 400, yn = 300 Maximale Werte x-Achse: +- 72, y-Achse +-40
 *
 */
public class Quadangle extends Point {

    // edges
    protected Vector[] v;

    // Constructor
    public Quadangle(Graphics initG, Vector[] fv) {
        super(initG, (int) fv[0].getAi(0) * 5 + xn,
                (int) (yn - fv[0].getAi(1) * 5));
        v = fv;
        visible = false;
    }

    public Quadangle turn4(double angle) {
        Vector[] tviereck = new Vector[4];

        tviereck[0] = v[0].turnH(angle);
        tviereck[1] = v[1].turnH(angle);
        tviereck[2] = v[2].turnH(angle);
        tviereck[3] = v[3].turnH(angle);
        return new Quadangle(g, tviereck);
    }

    public Quadangle stretch4(double x, double y) {
        Vector[] tviereck = new Vector[4];

        tviereck[0] = v[0].stretchH(x, y);
        tviereck[1] = v[1].stretchH(x, y);
        tviereck[2] = v[2].stretchH(x, y);
        tviereck[3] = v[3].stretchH(x, y);
        return new Quadangle(g, tviereck);
    }

    public Quadangle skew4(double y) {
        Vector[] tviereck = new Vector[4];

        tviereck[0] = v[0].skewH(y);
        tviereck[1] = v[1].skewH(y);
        tviereck[2] = v[2].skewH(y);
        tviereck[3] = v[3].skewH(y);
        return new Quadangle(g, tviereck);
    }

    public Quadangle mirrorX4() {
        Vector[] tviereck = new Vector[4];

        tviereck[0] = v[0].mirrorXH();
        tviereck[1] = v[1].mirrorXH();
        tviereck[2] = v[2].mirrorXH();
        tviereck[3] = v[3].mirrorXH();
        return new Quadangle(g, tviereck);
    }

    public Quadangle translate4(double[] t) {
        Vector[] tviereck = new Vector[4];

        tviereck[0] = v[0].translateH(t);
        tviereck[1] = v[1].translateH(t);
        tviereck[2] = v[2].translateH(t);
        tviereck[3] = v[3].translateH(t);
        return new Quadangle(g, tviereck);
    }

    // Method to draw the graphics
    // Multiplikation der Koordinaten mit 5 damit
    // das Viereck nicht so klein wird
    // Verschiebung der Koordinaten, damit Ursprung
    // etwa in Bildmitte
    public void draw() {

        int[] x = new int[4];
        int[] y = new int[4];

        x[0] = (int) v[0].getAi(0) * 5 + xn;
        y[0] = (int) (yn - v[0].getAi(1) * 5);
        x[1] = (int) v[1].getAi(0) * 5 + xn;
        y[1] = (int) (yn - v[1].getAi(1) * 5);
        x[2] = (int) v[2].getAi(0) * 5 + xn;
        y[2] = (int) (yn - v[2].getAi(1) * 5);
        x[3] = (int) v[3].getAi(0) * 5 + xn;
        y[3] = (int) (yn - v[3].getAi(1) * 5);

        if (visible) {
            g.setColor(foregroundColor);
        } else {
            g.setColor(backgroundColor);
        }
        g.fillPolygon(x, y, 4);
    }
}
