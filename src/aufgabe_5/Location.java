package aufgabe_5;

import java.awt.Graphics;

//This class Location defines screen location in x and y pixel coordinates
//for simple shapes within a Java-awt Graphics object "g"
public class Location {

    // Screensize
    static int xn = Picture.width / 2;
    static int yn = Picture.height / 2;

    //Attributes
    protected int x, y;
    protected static Graphics g;

    //Constructor
    public Location(Graphics initG, int initX, int initY) {
        g = initG;
        x = initX;
        y = initY;
    }

    //Methods to get coordinates
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
