package aufgabe_5;

import java.awt.Color;
import java.awt.Graphics;

//class Point paints a Point in a window
public class Point extends Location {

    //Attributes
    protected boolean visible;
    protected Color backgroundColor = Color.white;
    protected Color foregroundColor = Color.black;

    //Constructor
    public Point(Graphics initG, int initX, int initY) {
        super(initG, initX, initY);
        visible = false;
    }

    // Method to draw the graphics
    public void draw() {
        if (visible) {
            //Zeichne den Punkt
            g.setColor(foregroundColor);
        } else {
            //Zeichne Punkt in Hintergrundfarbe
            g.setColor(backgroundColor);
            //Must draw small Circle with Diameter=10 for visible Point !
        }
        g.fillOval(x - 11, y - 11, 20, 20);
    }

    // Visibility Methods
    public void setVisible() {
        visible = true;
        draw();
    }

    public void setVisible(boolean changedVisible) {
        visible = changedVisible;
        draw();
    }

    public void setInvisible() {
        visible = false;
        draw();
    }

    public boolean isVisible() {
        return visible;
    }

    //Methods to change location in window
    public void moveTo(int newx, int newy) {
        setInvisible();
        x = newx;
        y = newy;
        setVisible();
    }

    public void moveBy(int deltaX, int deltaY) {
        setInvisible();
        x = x + deltaX;
        y = y + deltaY;
        setVisible();
    }

    // Color Methods
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getForegroundColor() {
        return foregroundColor;
    }

    //New methods for P1, "OOD right from the start"
    public void setColor(Color changedColor) {
        foregroundColor = changedColor;
    }

    public void setColor(int colorHexcode) {
        //System.out.println("hex   "+colorHexcode);
        //Der Farbwert mussen zwischen 0x00000 und 0xFFFFFF liegen.
        foregroundColor = new Color(colorHexcode);
    }

    public void setColor(int ired, int igreen, int iblue) {
        //System.out.println("int   "+ired+" "+igreen+" "+iblue);
        //Die drei Farbwerte muuessen zwischen 0 und 255 liegen
        setColor(ired * 256 * 256 + igreen * 256 + iblue);
    }

    public void setColor(double dred, double dgreen, double dblue) {
        //Die drei Farbwerte muuessen zwischen 0.0 und 1.0 liegen
        int ired = (int) (dred * 255.);
        int igreen = (int) (dgreen * 255.);
        int iblue = (int) (dblue * 255.);
        setColor(ired, igreen, iblue);
    }

    public void setColor(String colorName) {
        // Expliziten Farbnamen asl String angegeben
        setColor(0x006600); //Dunkelguen als Default-Farbe (falls unbekannter Farbname)
        if (colorName.toLowerCase() == "black") {
            foregroundColor = Color.black;
        }
        if (colorName.toLowerCase() == "white") {
            foregroundColor = Color.white;
        }
        if (colorName.toLowerCase() == "red") {
            foregroundColor = Color.red;
        }
        if (colorName.toLowerCase() == "green") {
            foregroundColor = Color.green;
        }
        if (colorName.toLowerCase() == "blue") {
            foregroundColor = Color.blue;
        }
        if (colorName.toLowerCase() == "yellow") {
            foregroundColor = Color.yellow;
        }
        if (colorName.toLowerCase() == "cyan") {
            foregroundColor = Color.cyan;
        }
        if (colorName.toLowerCase() == "orange") {
            foregroundColor = Color.orange;
        }
        if (colorName.toLowerCase() == "magenta") {
            foregroundColor = Color.magenta;
        }
        if (colorName.toLowerCase() == "grey") {
            foregroundColor = Color.gray;
        }
        if (colorName.toLowerCase() == "gray") {
            foregroundColor = Color.gray;
        }
        if (colorName.toLowerCase() == "light_gray") {
            foregroundColor = Color.lightGray;
        }
        if (colorName.toLowerCase() == "dark_gray") {
            foregroundColor = Color.darkGray;
        }
        if (colorName.toLowerCase() == "lightgray") {
            foregroundColor = Color.lightGray;
        }
        if (colorName.toLowerCase() == "darkgray") {
            foregroundColor = Color.darkGray;
        }
        if (colorName.toLowerCase() == "light_grey") {
            foregroundColor = Color.lightGray;
        }
        if (colorName.toLowerCase() == "dark_grey") {
            foregroundColor = Color.darkGray;
        }
        if (colorName.toLowerCase() == "lightgrey") {
            foregroundColor = Color.lightGray;
        }
        if (colorName.toLowerCase() == "darkgrey") {
            foregroundColor = Color.darkGray;
        }
    }

    public void setSaturatedColor(int hsvWinkel) // siehe naechste Methode...
    {
        setColor((double) hsvWinkel);
    }

    public void setColor(double hsvWinkel) // Diese Methode erhaelt den Winkel einer voll gesaettigten Farbe
    // im HSV-Farbraum, d.h. nur auf dem oberen HSV-Konusrand.
    // Die Methode setzt die Farbe wie folgt:
    //	Hue        = hsvWinkel (Input-Variable)
    //    Saturation = 1.  (Fester Wert)
    //    Value      = 1.  (Fester Wert)
    {
        double winkel = hsvWinkel;
        double dred = 0., dgreen = 0., dblue = 0.;
        if (winkel >= 0. && winkel < 60.) {
            dred = 1.;
            dgreen = winkel / 60.;
            dblue = 0.;
        }
        if (winkel >= 60. && winkel < 120.) {
            dred = (120. - winkel) / 60.;
            dgreen = 1.;
            dblue = 0.;
        }
        if (winkel >= 120. && winkel < 180.) {
            dred = 0.;
            dgreen = 1.;
            dblue = (winkel - 120.) / 60.;
        }
        if (winkel >= 180. && winkel < 240.) {
            dred = 0.;
            dgreen = 1. - (winkel - 180.) / 60.;
            dblue = 1.;
        }
        if (winkel >= 240. && winkel < 300.) {
            dred = 1. - (300. - winkel) / 60.;
            dgreen = 0.;
            dblue = 1.;
        }
        if (winkel >= 300. && winkel < 360.) {
            dred = 1.;
            dgreen = 0.;
            dblue = (360. - winkel) / 60.;
        }
        setColor(dred, dgreen, dblue);
    }
}
