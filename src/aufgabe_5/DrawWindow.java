package aufgabe_5;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;

class DrawWindow extends Canvas {

    protected static final long serialVersionUID = 1;
    protected int width, height;
    protected Image offscreenImage;
    protected Graphics bufferGraphics;
    protected Graphics gBufferedImage;

    public DrawWindow(int initWidth, int initHeight) {
        super();
        width = initWidth;
        height = initHeight;
    }

    void coordinates(int width, int height) {
        FatLine f1 = new FatLine(bufferGraphics, 0, height / 2, width,
                height / 2, 2);
        f1.setVisible();
        FatLine f2 = new FatLine(bufferGraphics, width / 2, 0, width / 2,
                height, 2);
        f2.setVisible();
    }

    public void paint(Graphics g) {
        // Offscreen-Bild im Grafik-Puffer vorbereiten
        offscreenImage = createImage(width, height);
        bufferGraphics = offscreenImage.getGraphics();

        coordinates(Picture.width, Picture.height);

        // Ein Viereck zeichnen:

        /**/
        Vector[] viereck = new Vector[4];
        viereck[0] = new Vector(bufferGraphics, 5.0, 30.0);
        viereck[1] = new Vector(bufferGraphics, 5.0, 5.0);
        viereck[2] = new Vector(bufferGraphics, 15.0, 5.0);
        viereck[3] = new Vector(bufferGraphics, 15.0, 30.0);

        Quadangle v1 = new Quadangle(bufferGraphics, viereck);
        v1.setVisible();
        Quadangle v2, v3, v4, v5, v6;

        double[] t = new double[2];
        t[0] = 20;
        t[1] = 15;

        v2 = v1.translate4(t);
        v2.setColor("blue");
        v2.setVisible();

        v3 = v2.mirrorX4();
        v3.setColor("red");
        v3.setVisible();

        v4 = v3.turn4(Math.PI / 4);
        v4.setColor("green");
        v4.setVisible();

        v5 = v3.stretch4(-2, -0.5);
        v5.setColor("yellow");
        v5.setVisible();

        v6 = v5.skew4(2);
        v6.setColor("brown");
        v6.setVisible();

        /**/
        /*
         Vector[] dreieck = new Vector[3];
         dreieck[0] = new Vector(bufferGraphics, 5.0, 30.0);
         dreieck[1] = new Vector(bufferGraphics, 5.0, 5.0);
         dreieck[2] = new Vector(bufferGraphics, 15.0, 5.0);

         Triangle t1 = new Triangle(bufferGraphics, dreieck);
         t1.setVisible();

         Triangle t2, t3, t4, t5, t6;

         double[] tt = new double[2];
         tt[0] = 20;
         tt[1] = 15;

         t2 = t1.translate3(tt);
         t2.setColor("blue");
         t2.setVisible();

         t3 = t2.mirrorX3();
         t3.setColor("red");
         t3.setVisible();

         t4 = t3.turn3(Math.PI / 4);
         t4.setColor("green");
         t4.setVisible();

         t5 = t3.stretch3(2, 0.5);
         t5.setColor("yellow");
         t5.setVisible();

         t6 = t5.skew3(2);
         t6.setColor("brown");
         t6.setVisible();
         /**/
        // Bild zeichnen: letzte Zeile der Paint Methode
        g.drawImage(offscreenImage, 0, 0, this);
    }
}
