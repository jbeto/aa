package aufgabe_5;

import java.awt.Graphics;

/**
 * *
 *
 * Zeichnet ein Dreieck in ein (unsichtbares) Koordinatensystem mit Ursprung in
 * xn = 400, yn = 300 Maximale Werte x-Achse: +- 72, y-Achse +-40
 *
 */
public class Triangle extends Point {

    // edges
    protected Vector[] v;

    // Constructor
    public Triangle(Graphics initG, Vector[] fv) {
        super(initG, (int) fv[0].getAi(0) * 5 + xn,
                (int) (yn - fv[0].getAi(1) * 5));
        v = fv;
        visible = false;
    }

    public Triangle turn3(double angle) {
        Vector[] tdreieck = new Vector[3];

        tdreieck[0] = v[0].turnH(angle);
        tdreieck[1] = v[1].turnH(angle);
        tdreieck[2] = v[2].turnH(angle);
        return new Triangle(g, tdreieck);
    }

    public Triangle stretch3(double x, double y) {
        Vector[] tdreieck = new Vector[3];

        tdreieck[0] = v[0].stretchH(x, y);
        tdreieck[1] = v[1].stretchH(x, y);
        tdreieck[2] = v[2].stretchH(x, y);
        return new Triangle(g, tdreieck);
    }

    public Triangle skew3(double y) {
        Vector[] tdreieck = new Vector[3];

        tdreieck[0] = v[0].skewH(y);
        tdreieck[1] = v[1].skewH(y);
        tdreieck[2] = v[2].skewH(y);
        return new Triangle(g, tdreieck);
    }

    public Triangle mirrorX3() {
        Vector[] tdreieck = new Vector[3];

        tdreieck[0] = v[0].mirrorXH();
        tdreieck[1] = v[1].mirrorXH();
        tdreieck[2] = v[2].mirrorXH();
        return new Triangle(g, tdreieck);
    }

    public Triangle translate3(double[] t) {
        Vector[] tdreieck = new Vector[3];

        tdreieck[0] = v[0].translateH(t);
        tdreieck[1] = v[1].translateH(t);
        tdreieck[2] = v[2].translateH(t);
        return new Triangle(g, tdreieck);
    }

    // Method to draw the graphics
    // Multiplikation der Koordinaten mit 5 damit
    // das Dreieck nicht so klein wird
    // Verschiebung der Koordinaten, damit Ursprung
    // etwa in Bildmitte
    public void draw() {

        int[] x = new int[3];
        int[] y = new int[3];

        x[0] = (int) v[0].getAi(0) * 5 + xn;
        y[0] = (int) (yn - v[0].getAi(1) * 5);
        x[1] = (int) v[1].getAi(0) * 5 + xn;
        y[1] = (int) (yn - v[1].getAi(1) * 5);
        x[2] = (int) v[2].getAi(0) * 5 + xn;
        y[2] = (int) (yn - v[2].getAi(1) * 5);

        if (visible) {
            g.setColor(foregroundColor);
        } else {
            g.setColor(backgroundColor);
        }
        g.fillPolygon(x, y, 3);
    }
}
