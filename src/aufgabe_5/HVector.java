package aufgabe_5;

import java.awt.Graphics;

public class HVector extends Vector {

    public HVector(Graphics initG, Vector vector) {

        super(initG, vector.a);
        int i = 0;
        dim = vector.getDim() + 1;
        a = new double[dim];
        while (i < dim - 1) {
            a[i] = vector.getAi(i);
            i++;
        }
        a[dim - 1] = 1;
    }

}
