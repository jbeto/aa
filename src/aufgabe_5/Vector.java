// Implementierung fehlender Matrizen: Edgar Toll
package aufgabe_5;

import java.awt.Graphics;
import java.io.BufferedReader;
import java.util.Arrays;
import myIOPackage.MyIO;

public class Vector extends Point {

    // in dem folgenden Teil muessen Sie nix machen!!
    // //////////////////////////////////////////////
    public double[] a;
    protected int dim;
    private static BufferedReader In;

    public Vector(Graphics initG, double[] values) {
        super(initG, (int) values[0] * 5 + xn, (int) (yn - values[1] * 5));
        int i = 0;
        dim = values.length;
        a = new double[dim];
        while (i < dim) {
            a[i] = values[i++];
        }
    }

    public Vector(Graphics initG, double x, double y) {
        super(initG, (int) x * 5 + xn, (int) (yn - y * 5));
        dim = 2;
        a = new double[dim];
        a[0] = x;
        a[1] = y;
    }

    public Vector add(Vector aVector) {
        if (dim != aVector.getDim()) {
            return null;
        } else {
            double[] t = new double[dim];
            int i = 0;
            while (i < dim) {
                t[i] = a[i] + aVector.getA()[i];
                i++;
            }
            return new Vector(g, t);
        }
    }

    public Vector sub(Vector aVector) {
        if (dim != aVector.getDim()) {
            return null;
        } else {
            double[] t = new double[dim];
            int i = 0;
            while (i < dim) {
                t[i] = a[i] - aVector.getA()[i];
                i++;
            }
            return new Vector(g, t);
        }
    }

    public double scalarProduct(Vector aVector) {
        if (dim != aVector.getDim()) {
            return 0;
        } else {
            int i = 0;
            double p = 0;
            while (i < dim) {
                p = p + a[i] * aVector.getA()[i];
                i++;
            }
            return p;
        }
    }

    public double norm() {
        return Math.sqrt(scalarProduct(this));
    }

    public Vector scalarMult(double s) {
        double[] t = new double[dim];
        int i = 0;
        while (i < dim) {
            t[i] = a[i] * s;
            i++;
        }
        return new Vector(g, t);
    }

    public static Vector readVector(String n) {
        int vdim = MyIO.readInt("Dimension", In);

        double[] v = new double[vdim];
        int i = 0;

        while (i < vdim) {
            v[i] = MyIO.readDouble(n + "[" + i + "]", In);
            i++;
        }
        return new Vector(g, v);
    }

    public static double distanceToLine(Vector a, Vector b, Vector P) {
        double lambda;
        Vector t;
        lambda = (P.scalarProduct(a) - b.scalarProduct(a)) / a.scalarProduct(a);
        t = a.scalarMult(lambda).add(b).sub(P);
        return Math.sqrt(t.scalarProduct(t));
    }

    public static double angle(Vector u, Vector v) {
        double cosphi;
        cosphi = u.scalarProduct(v) / (u.norm() * v.norm());
        return Math.acos(cosphi) * 180.0 / Math.PI;
    }

    public double getAi(int i) {
        return a[i];
    }

    public double[] getA() {
        return Arrays.copyOf(a, dim);
    }

    public int getDim() {
        return dim;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Arrays.hashCode(this.a);
        hash = 73 * hash + this.dim;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vector other = (Vector) obj;
        if (this.dim != other.dim) {
            return false;
        }
        return Arrays.equals(this.a, other.a);
    }

    @Override
    public String toString() {
        String s = "(";
        int i = 0;
        while (i < dim) {
            s = s + a[i];
            i++;
            if (i == dim) {
                s = s + ")";
            } else {
                s = s + ",";
            }
        }
        return s;
    }

    @Override
    public void draw() {

        int[] x = new int[2];
        int[] y = new int[2];

        x[0] = xn;
        y[0] = yn;
        x[1] = (int) a[0] * 5 + xn;
        y[1] = (int) (yn - a[1] * 5);

        FatLine f1 = new FatLine(g, x[0], y[0], x[1], y[1], 4);
        f1.setVisible();
    }

	//Die folgende main routine wurde fuer die letzte Aufgabe benoetigt
	/*
     public static void main(String[] args) {

     System.out.println("Vector 0.0");
     Vector u, v;

     In = new BufferedReader(new InputStreamReader(System.in));

     System.out.println("u einlesen"); // u = (1.0, 2.0, 3.0)
     u = readVector("u");
     System.out.println("v einlesen"); // v = (4.0, 5.0, 6.0)
     v = readVector("v");
     System.out.println(u);
     System.out.println(v);
     System.out.println("u+v = " + u.add(v));
     System.out.println("u-v = " + u.sub(v));
     System.out.println("skalare Multiplikation 5.0*u = "
     + u.scalarMult(5.0));
     System.out.println("Skalarprodukt u*v = " + u.scalarProduct(v));
     System.out.println("Norm(u) = " + u.norm());
     System.out.println("Norm(v) = " + v.norm());
     System.out.println();
     System.out
     .println("Abstand des Punktes P zur Geraden x = lambda * a + b");
     Vector a, b, P;
     System.out.println("a einlesen"); // a = (1.0, 1.0)
     a = readVector("a");
     System.out.println("b einlesen"); // b = (0.0, 0.0)
     b = readVector("b");
     System.out.println("P einlesen"); // P = (0.0, 1.0)
     P = readVector("P");
     System.out.println("Abstand = " + distanceToLine(a, b, P));
     System.out.println();
     System.out.println("Winkel zwischen zwei Vektoren");
     System.out.println("u einlesen"); // u = (1.0, 0.0, 0.0)
     u = readVector("u");
     System.out.println("v einlesen"); // v = (0.0, 1.0, 0.0)
     v = readVector("v");
     System.out.println("Winkel = " + angle(u, v));
     }
     */
    // ab hier geht es los
    // //////////////////////////////////////////////
    public static double[][] genIdMatrix(int hdim) {

        double[][] tmatrix = new double[hdim][hdim];

        for (int x = 0; x < hdim; x++) {
            for (int y = 0; y < hdim; y++) {
                if (x == y) {
                    tmatrix[x][y] = 1;
                } else {
                    tmatrix[x][y] = 0;
                }
            }
        }
        // hier erzeugen Sie eine Einheitsmatrix, alles 0 nur Hauptdiagonale 1
        return tmatrix;
    }

    public double[] mulMatHVect(double[][] tmatrix, HVector hv) {

        // tmatrix wird mit hv multipliziert
        // Resultat steht in ta
        double[] ta = new double[hv.getDim()];
        double[] hvA = hv.getA();

        for (int y = 0; y < ta.length; y++) {
            double result = 0;

            for (int x = 0; x < ta.length; x++) {
                double currentVectorValue = hvA[x];
                double currentMatrixValue = tmatrix[y][x];

                result += currentVectorValue * currentMatrixValue;
            }

            ta[y] = result;
        }

        for (int i = 0; i < ta.length - 1; i++) {
            ta[i] /= ta[ta.length - 1];
        }
        ta[ta.length - 1] = 1;

        return Arrays.copyOf(ta, ta.length - 1);
    }

    public Vector turnH(double angle) {

        HVector hv = new HVector(g, this);

        double[][] matrix3 = new double[][]{
            {Math.cos(angle), -Math.sin(angle), 0},
            {Math.sin(angle), Math.cos(angle), 0},
            {0, 0, 1}
        };
        double[] ta;

        // Transformationsmatrix matrix3 definieren
        ta = mulMatHVect(matrix3, hv);

        System.out.println("Turn " + angle + ": alt = " + a[0] + " " + a[1]
                + " neu: " + ta[0] + " " + ta[1]);
        return new Vector(g, ta);
    }

    public Vector stretchH(double x, double y) {
        HVector hv = new HVector(g, this);
        double[][] matrix3 = genIdMatrix(3);
        double[] ta;

        matrix3[0][0] = x;
        matrix3[1][1] = y;

        // Transformationsmatrix matrix3 definieren
        ta = mulMatHVect(matrix3, hv);
        System.out.println("Stretch " + " " + x + " " + y + ": alt = " + a[0]
                + " " + a[1] + " neu: " + ta[0] + " " + ta[1]);

        return new Vector(g, ta);
    }

    public Vector skewH(double y) {
        HVector hv = new HVector(g, this);
        double[][] matrix3 = genIdMatrix(3);
        double[] ta;

        matrix3[0][1] = y;

        // Transformationsmatrix matrix3 definieren
        ta = mulMatHVect(matrix3, hv);
        System.out.println("Skew " + y + ": alt = " + a[0] + " " + a[1]
                + " neu: " + ta[0] + " " + ta[1]);

        return new Vector(g, ta);
    }

    public Vector mirrorXH() {
        HVector hv = new HVector(g, this);
        double[][] matrix3 = genIdMatrix(3);
        double[] ta;

        matrix3[1][1] = -1;

        // Transformationsmatrix matrix3 definieren
        ta = mulMatHVect(matrix3, hv);
        System.out.println("MirrorX : alt = " + a[0] + " " + a[1] + " neu: "
                + ta[0] + " " + ta[1]);

        return new Vector(g, ta);
    }

    public Vector translateH(double[] t) {

        double[] ta;
        double[][] tmatrix = genIdMatrix(t.length + 1);

        for (int i = 0; i < t.length; i++) {
            tmatrix[i][t.length] = t[i];
        }

        HVector hv = new HVector(g, this);

        // Translationsmatrix tmatrix basteln, t enthaelt die Koordinaten fuer
        // die Verschiebung
        // Multiplikation Matrix * Vektor
        ta = mulMatHVect(tmatrix, hv);
        System.out.println("Translate " + t[0] + " " + t[1] + " : alt = "
                + a[0] + " " + a[1] + " neu: " + ta[0] + " " + ta[1]);
        return new Vector(g, ta);
    }
}
