package aufgabe_5;

//CloseWindow.java; Interaktives Schliessen der Grafik
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CloseWindow extends WindowAdapter {

    public void windowClosing(WindowEvent we) {
        // System.out.println("Entering windowClosing()");
        // System.out.println("Before System.exit(0) in  windowClosing()  !!!!!");
        System.exit(0);
        // System.out.println("After System.exit(0) in  windowClosing()  !!!!!");

    }
}
