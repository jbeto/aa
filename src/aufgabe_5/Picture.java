package aufgabe_5;

// Klasse "Picture":
// 1. definiert das leere Bild-Fenster (= Window = Picture-Frame)
// 2. ruft Konstruktor der Zeichnung ("DrawWindow") auf,
// 3. und macht alles sichtbar.
import java.awt.Frame;

public class Picture extends Frame {

    static final long serialVersionUID = 1;
    public static int width = 800, height = 600, left = 150, top = 50;

    public Picture() {
        //Das leere Fenster
        super();
        setBounds(left, top, width, height);
        addWindowListener(new CloseWindow());

        //Der INHALT des Fensters (= "DrawWindow")
        DrawWindow d = new DrawWindow(width, height);
        add(d);
        setVisible(true);
    }
}
