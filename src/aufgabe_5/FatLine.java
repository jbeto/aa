package aufgabe_5;

import java.awt.Graphics;

public class FatLine extends Point {

    //Attributes
    protected int[] x = new int[5];
    protected int[] y = new int[5];
    protected int width;

    //Constructor
    public FatLine(Graphics initG, int initX1, int initY1,
            int initX2, int initY2, int initWidth) // Constructor interface:
    // initX1, initY1: middle of starting point of fat line
    // initX2, initY2: middle of ending point of fat line
    // initWidth: width of the fat line
    // All Methods of class "Point" are available, e.g. "setSaturatedColor(...)", etc.
    {
        super(initG, initX1, initY1);
        if (initWidth < 0) {
            initWidth = 0;  //Negative Linienbreite verboten, null ABER nicht
        }
        width = initWidth;
        int xWidth, yWidth, signSlope = 1;;
        if (initX2 == initX1) // waagerechte Linie
        {
            yWidth = 0;
            xWidth = width / 2;
        } else {
            if (initY2 == initY1) //senkrechte Linie
            {
                yWidth = width / 2;
                xWidth = 0;
            } else {
                double slope = ((double) (initY2 - initY1)) / ((double) (initX2 - initX1));
                signSlope = (int) (Math.abs(slope) / slope);
                double slope2 = -1. / slope;
                xWidth = ((int) ((double) width * Math.sqrt(1. / (1. + slope2 * slope2)))) / 2;
                yWidth = ((int) ((double) width * Math.sqrt(1. / (1. + 1. / (slope2 * slope2))))) / 2;
            }
        }
        x[0] = initX1 + signSlope * xWidth;
        y[0] = initY1 - yWidth;
        x[1] = initX2 + signSlope * xWidth;
        y[1] = initY2 - yWidth;
        x[2] = initX2 - signSlope * xWidth;
        y[2] = initY2 + yWidth;
        x[3] = initX1 - signSlope * xWidth;
        y[3] = initY1 + yWidth;
        x[4] = x[0];
        y[4] = y[0];

        //visible=false;
    }

    // Method to draw the graphics
    public void draw() {
        if (visible) {
            g.setColor(foregroundColor);
        } else {
            g.setColor(backgroundColor);
        }
        g.fillPolygon(x, y, 5);
    }
}
