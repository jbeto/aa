package myIOPackage;

import java.io.BufferedReader;
import java.io.IOException;

public class MyIO {

    /////////////////////////////////////////////////////////////////////
    public static int readInt(String prompt, BufferedReader In) {
        Integer i;
        int res = 0;
        String expr;
        try {
            System.out.print(prompt + " = ");
            expr = (In.readLine());
            i = new Integer(expr);
            res = i.intValue();
        } catch (IOException e) {
            System.out.println("Problem mit Eingabe");
            System.exit(-1);
        };
        return res;
    }

    /////////////////////////////////////////////////////////////////////
    public static double readDouble(String prompt, BufferedReader In) {
        Double d;
        double res = 0;
        String expr;
        try {
            System.out.print(prompt + " = ");
            expr = (In.readLine());
            d = new Double(expr);
            res = d.doubleValue();
        } catch (IOException e) {
            System.out.println("Problem mit Eingabe");
            System.exit(-1);
        };
        return res;
    }

    /////////////////////////////////////////////////////////////////////
    public static String readString(String prompt, BufferedReader In) {
        String str = "";
        try {
            System.out.print(prompt + " = ");
            str = (In.readLine());
        } catch (IOException e) {
            System.out.println("Problem mit Eingabe");
            System.exit(-1);
        };
        return str;
    }

}
