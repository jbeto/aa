// Implementierung: Edgar Toll
package aufgabe_3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Vector {

    private final double[] a;
    private final int dim;
    private static BufferedReader In;

    public Vector() {

// hier soll der Standardkonstruktor stehen
        a = new double[0];
        dim = 0;
    }

    public Vector(double... values) {

// Dieser Konstruktor hat als Parameter ein dynamisches array,
// das er in einen Vektor mit der passenden Dimension verwandelt.
// Die Länge von "values" erhalten Sie mit "values.length".
        dim = values.length;
        a = Arrays.copyOf(values, dim);
    }

    public Vector add(Vector aVector) {

// Der Vektor "aVector" soll zu this addiert werden.
// Das Ergebnis wird abgeliefert, this bleibt unverändert!
        double[] returnA = new double[dim];

        double[] otherA = aVector.getA();
        for (int i = 0; i < dim; i++) {
            returnA[i] = a[i] + otherA[i];
        }

        return new Vector(returnA);
    }

    public Vector sub(Vector aVector) {

// Der Vektor "aVector" soll von this subtrahiert werden.
// Das Ergebnis wird abgeliefert, this bleibt unverändert!
        return add(aVector.scalarMult(-1));
    }

    public double scalarProduct(Vector aVector) {

// Skalarprodukt von this und "aVector" wird zurückgegeben
        double returnValue = 0;

        double[] otherA = aVector.getA();
        for (int i = 0; i < dim; i++) {
            returnValue += a[i] * otherA[i];
        }

        return returnValue;
    }

    public double norm() {

// die Norm von "this" wird zurückgegeben
        double bla = 0;

        for (int i = 0; i < dim; i++) {
            bla += Math.pow(a[i], 2);
        }

        return Math.sqrt(bla);
    }

    public Vector scalarMult(double s) {

// Als Ergebnis wird this * s berechnet und zurückgegeben. this bleibt unverändert!
        double[] returnA = new double[dim];

        for (int i = 0; i < dim; i++) {
            returnA[i] = a[i] * s;
        }

        return new Vector(returnA);
    }

    public double[] getA() {
        return Arrays.copyOf(a, dim);
    }

    public int getDim() {
        return dim;
    }

    @Override
    public String toString() {

// konvertiert this in einen String der Form "(a[0], ..., a[n])"
// siehe trace
        String returnString = "(";

        for (int i = 0; i < a.length; i++) {
            if (i > 0) {
                returnString += ",";
            }

            returnString += Double.toString(a[i]);
        }

        return returnString + ")";
    }

    public static Vector readVector(String n) {

// liest erst die Dimension von "n" ein und dann die Komponenten
// siehe trace
        int dim = myIOPackage.MyIO.readInt("Dimension", In);
        double[] a = new double[dim];

        for (int i = 0; i < dim; i++) {
            a[i] = myIOPackage.MyIO.readDouble(String.format("%s[%d]", n, i), In);
        }

        return new Vector(a);
    }

    public static double distanceToLine(Vector a, Vector b, Vector P) {

// haben wir noch nicht gemacht: "not yet impleneted" ausgeben
// Sie dürfen aber mit vorauseilendem Gehorsam auch ins Skript
// und die Operation implemetieren
// berechnet die Entfernung des Punktes P zu Geraden g = lambda * a + b
// siehe Aufgabenblatt
        double lambda = (P.scalarProduct(a) - a.scalarProduct(b)) / a.scalarProduct(a);
        Vector x = a.scalarMult(lambda).add(b).sub(P);
        double y = x.scalarProduct(x);
        double z = Math.sqrt(y);
        return z;
    }

    public static double angle(Vector u, Vector v) {

// berechnet den Winkel zwischen u und v in Grad, siehe Skript
        return Math.acos(u.scalarProduct(v) / (u.norm() * v.norm())) * (180 / Math.PI);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Arrays.hashCode(this.a);
        hash = 73 * hash + this.dim;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vector other = (Vector) obj;
        if (this.dim != other.dim) {
            return false;
        }
        return Arrays.equals(this.a, other.a);
    }

    public static void main(String[] args) {

        System.out.println("Vector 0.0");
        Vector u, v;

        In = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("u einlesen"); // u = (1.0, 2.0, 3.0)
        u = readVector("u");
        System.out.println("v einlesen"); // v = (4.0, 5.0, 6.0)
        v = readVector("v");
        System.out.println(u);
        System.out.println(v);
        System.out.println("u+v = " + u.add(v));
        System.out.println("u-v = " + u.sub(v));
        System.out.println("skalare Multiplikation 5.0*u = "
                + u.scalarMult(5.0));
        System.out.println("Skalarprodukt u*v = " + u.scalarProduct(v));
        System.out.println("Norm(u) = " + u.norm());
        System.out.println("Norm(v) = " + v.norm());
        System.out.println();
        System.out
                .println("Abstand des Punktes P zur Geraden x = lambda * a + b");
        Vector a, b, P;
        System.out.println("a einlesen"); // a = (1.0, 1.0)
        a = readVector("a");
        System.out.println("b einlesen"); // b = (0.0, 0.0)
        b = readVector("b");
        System.out.println("P einlesen"); // P = (0.0, 1.0)
        P = readVector("P");
        System.out.println("Abstand = " + distanceToLine(a, b, P));
        System.out.println();
        System.out.println("Winkel zwischen zwei Vektoren");
        System.out.println("u einlesen"); // u = (1.0, 0.0, 0.0)
        u = readVector("u");
        System.out.println("v einlesen"); // v = (0.0, 1.0, 0.0)
        v = readVector("v");
        System.out.println("Winkel = " + angle(u, v));
    }

}
